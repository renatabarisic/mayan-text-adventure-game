using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class StoryText : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] TMP_Text textStory;
    [SerializeField] State startingState;

    State state;
    void Start()
    {
        state = startingState;
        textStory.text = state.GetStateStory();
    }

    // Update is called once per frame
    void Update()
    {
        ManageStates();
        
    }

    private void ManageStates()
    {
        var nextStates = state.GetNextStates();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            state = nextStates[0];
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            state = nextStates[1];
        }
        textStory.text = state.GetStateStory();
    }
}
